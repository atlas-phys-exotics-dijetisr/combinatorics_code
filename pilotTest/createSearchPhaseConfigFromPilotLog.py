#convergesmport create
from runFunctions import spectrumGlobalFit
def functionNParamsLookUp(functionCode): # this is the functionCode from gptoy
    """using the function code , look up how many parameter that fit function has!"""
    codeToNParamDictionary={"0": 4, "1": 4, "2": 3, "3": 5}
    return codeToNParamDictionary[str(functionCode)]

def outputSearchPhaseConfigName(seriesName, fitRange, functionCode):
    """ generate output searchPhase config file names"""
    print("fitRange[0]", fitRange[0])
    print("fitRange[1]", fitRange[1])
    print("seriesName:", seriesName)
    name=seriesName+"_fitRange"+str(fitRange[0])+"-"+str(fitRange[1])+"_functionCode"+str(functionCode)+".config"
    print("searchPhaseconfigName: ", name)
    return name

def outputDictKey(seriesName, fitRange, functionCode):
    """ generate output searchPhase config file names"""
    return seriesName+"_functionCode"+functionCode

def seriesNameFromLine(line):
    """ series name from the ---Log line"""
    pos0=line.find("for ")+len("for ")
    pos1=pos0+ line.find("_fitRange")-len("_fitRange")-3
    seriesName=line[pos0:pos1]
    print("seriesName:", seriesName)
    return seriesName

bestFitList={} # sort the best fits by the SeriesName and fitFunction type
def createSearchPhaseConfigFromPilotLog(config):
    pilotGoodConfigList=[]
    fitRange=[0,0]
    functionCode=0
    nParams=0
    with open(config["inputPilotTxt"], 'r') as input_pilot:
        # open the template file write everything that is not the param, the fit function code, doAlt param in here
        listOfOmittingKeywords={"permitWindow": "False", "minXForFit":fitRange[0] , "maxXForFit": fitRange[0], "nPseudoExpFit":1, "functionCode":functionCode, "nParameters":nParams, "doAlternateFunction":False, "doSwift":True, "swift_minXAvailable":fitRange[0], "swift_maxXAvailable":5000, "parameter":0}
        for line in input_pilot:
            if "---Log" in line: # header line
                badFit=False
                #---find seriesName for searchPhase config name
                seriesName=seriesNameFromLine(line)
                #---find rootFileName
                rootName=rootNameFromLine(line)
                #--find the fit range from the log name (resetting the correct value)
                fitRange, functionCode=fitRangeFitFunctionFromLine(line)
                nParams=functionNParamsLookUp(functionCode)
                #dictKey=outputDictKey(seriesName, functionCode)
                #if not dictKey in bestFitList.keys():
                input_template=open(config["inputSearchPhaseTemplate"], "r")
                searchPhaseConfig=outputSearchPhaseConfigName(seriesName, fitRange, functionCode)
                output_file=open(config["outputConfigDir"]+"/"+searchPhaseConfig, "w+")
                #---find fit Range and fitFunction code
                for sptemplateLine in input_template:
                    # case 1: key is in the omit list and is not parameter
                    if any(sptemplateLine.startswith(key) and not key=="parameter" for key in listOfOmittingKeywords.keys()):
                        output_file.write("{0}   {1}\n".format(sptemplateLine.split()[0], sptemplateLine.split()[0]))
                    # case 2: key is in the omit list and IS paramter
                    elif any(sptemplateLine.startswith(key) and key=="parameter" for key in listOfOmittingKeywords.keys()):
                        pass # the parameters will be written to the output fiel from the pilot txt, do nothing
                    else: # if sptemplatate does not start with the word list in omit key word
                        output_file.write(sptemplateLine) # writing things directly
            elif line.startswith("parameter"): #case 2
                output_file.write(line)
            elif line.startswith("---------------------------"): # ending condition # does this work?
                input_template.close()
                output_file.close()
                pilotGoodConfigList.append(searchPhaseConfig)

def rootNameFromLine(line):
    """determines the searchPhase config file name"""
    pos0=line.find("Log")
    pos1=line[pos0:].find("---")
    name=line[pos0:pos1]+".root"
    print(name)
    return name

def rootFileNameFromLine(line):
    """determines the rootFileName"""
    pos0=line.find("Log")
    pos1=line[pos0:].find("fitRange")
    name=line[pos0:pos1]
    print(name)
    return name

def fitRangeFitFunctionFromLine(line):
    """determines the fit range and fit function code"""
    # find "fitRange"
    pos0=line.find("fitRange")+len("fitRange")
    # find "-"
    pos1 = line[pos0:].find("-")+pos0
    # find "_"
    pos2 = line[pos1:].find("_")+pos1
    # find "-"
    pos3 = line[pos2:].find("-")+pos2
    fitRange=[line[pos0:pos1], line[pos1+1: pos2]]
    functionType=line[pos2+1:pos3]
    print ("fitRange:", fitRange)
    print("function Type: ", functionType)
    functionCode=[i for i in range(len(spectrumGlobalFit.fitFunctionFromCode)) if functionType==spectrumGlobalFit.fitFunctionFromCode[i]][0]
    print("function Code: ", functionCode)
    return fitRange, functionCode

def searchPhaseConfigNameGeneration(seriesName,functionCode):
    """Search Phase config name generation"""
    name="searchPhase_"+seriesName+"_nunctionCode"+functionCode+".config"
    return name



if __name__=="__main__":

    configCreateSearchPhaseConfig={"inputPilotTxt": "pilotStudiesMC16adMay2018.txt",
                               "outputConfigDir": "outputSearchPhaseConfig/",
                               "inputSearchPhaseTemplate": "/lustre/SCRATCH/atlas/ywng/WorkSpace/r21/r21SwiftClean/swiftLimitSetting/r21SwiftNew/SensitivityStudies/source/scripts/configurations/Step1_SearchPhase_Swift_trijet_j380_inclusive.config"}

    goodListConfig=createSearchPhaseConfigFromPilotLog(configCreateSearchPhaseConfig)
   # run SearchPhase in batch
    config={"inputRootDir":"",
           "goodListConfig": "",
           "doSwift": ""}
    # loop through the swift windows and run this in batches
    #runSearchPhaseBatchLog=runSearchPhaseInBatch()
    # log should store the p value and whether it converges
