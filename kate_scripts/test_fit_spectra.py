import ROOT
import sys, os
import glob
import math
import subprocess
from condor_files import CondorHandler

## What do you want to do?
doSWIFTFits = True
doGlobalFits = True
useBatch = False
# Options: "data", "mc"
fitSample = "data"


## Various general settings
#inputDir = "/afs/cern.ch/work/k/kpachal/DijetISR/Resolved2017/LimitSetting/FittingInputs/"
if "mc" in fitSample :
  inputDir = "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijetISR/histograms/fitting_inputs_Gang/"
  outputDir = "/afs/cern.ch/work/k/kpachal/DijetISR/Resolved2017/LimitSetting/BayesianFramework/results/testFitsMC16aMC16d/"
  inHistName = "background_mjj_var"

elif "data" in fitSample :
  inputDir = "/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/dijetISR/histograms/fitting_inputs_Gang/"
  outputDir = "/afs/cern.ch/work/k/kpachal/DijetISR/Resolved2017/LimitSetting/BayesianFramework/results/testFits15ifb/"
  inHistName = "background_mjj_var"

else :
  print "Unrecognized option",fitSample,"!"
  exit(0)

print "Seeking inHistName",inHistName

## Set up Condor handler
install_path = "/afs/cern.ch/work/k/kpachal/DijetISR/Resolved2017/LimitSetting/BayesianFramework/build/x86_64-slc6-gcc62-opt/bin/"
log_path = "/afs/cern.ch/work/k/kpachal/DijetISR/Resolved2017/LimitSetting/BayesianFramework/run/logs/"
batch_path = "/afs/cern.ch/work/k/kpachal/DijetISR/Resolved2017/LimitSetting/BayesianFramework/run/batch/"
batchmanager = CondorHandler(install_path, log_path, batch_path)

minXDict = {"dijetgamma_single_sktrigger" : 168,
            "dijetgamma_compound_trigger" : 300,
            "trijet" : 300}

maxXForFit = 1200

minWHW = 9

functionLoopDict = {
  "fivepar" : {
    "functioncode" : 7,
    "npar" : 5
  },
  "fourpar" : {
    "functioncode" : 4,
    "npar" : 4
  },
  "UA2" : {
    "functioncode" : 1,
    "npar" : 4
  }
}

# Dictionary of good start parameters.
# Based on fits to MC with equivalent luminosities of 
# 79.826 dijetgamma
# 79.521 trijet
if "mc" in fitSample :
  from start_pars_80fb import startParDict
else :
  from start_pars_15fb import startParDict

templateConfig = os.path.realpath("../source/Bayesian/configurations/DijetISR_Configs/Step1_SearchPhase_Swift_template.config")
newConfigDir = os.path.realpath("../source/Bayesian/configurations/DijetISR_Configs/")

## Cycle through 4 spectra
for channel in ["trijet", "dijetgamma_compound_trigger","dijetgamma_single_trigger"] :
  for ntag in ["inclusive", "nbtag2"] :
    # Cycle through acceptable functions
    for function in functionLoopDict.keys() :

      # Line up input file
      searchChannel = channel
      if "data" in fitSample :
        searchChannel = searchChannel.replace("dijetgamma_single_trigger","dijetgamma_HLT_g140_loose")
        searchChannel = searchChannel.replace("dijetgamma_compound_trigger","dijetgamma_HLT_g75_tight_3j50noL1")
      searchPattern = inputDir+"{0}*{1}*.root".format(searchChannel,ntag)
      inputFileList = glob.glob(searchPattern)
      # Throw out data or mc accordingly
      if not "data" in fitSample :
        inputFileList = [i for i in inputFileList if not "data" in i]
      if not "mc" in fitSample :
        inputFileList = [i for i in inputFileList if "data" in i]
      # Keep only correct ystar cut
      inputFileList = [i for i in inputFileList if "ystar0p75" in i or "ystar075" in i]
      if len(inputFileList) > 1 :
        print("More than 1 file found!")
        print(inputFileList)
        exit(0)
      # No 2b at present in data :
      if len(inputFileList) == 0 :
        print "No files matching",searchPattern
        continue
    
      inputFile = inputFileList[0]
      print "Fitting to",inputFile
      openFile = ROOT.TFile.Open(inputFile,"READ")
      inHist = openFile.Get(inHistName)
      inHist.SetDirectory(0)
      openFile.Close()

      # Min x for fit by channel, max defined at top
      simplechannel = channel.split("_")[0]
      minXForFit = minXDict[channel]
      minBinX = inHist.FindBin(minXForFit)+1

      maxBinX = inHist.FindBin(maxXForFit)-1

      # Max x available defined by last bin with > 1 event in histogram.
      maxXAvailable = 7000
      maxBinAvailable = inHist.GetNbinsX()
      for bin in range(inHist.GetNbinsX(), 1, -1) :
        if inHist.GetBinContent(bin) > 1.0 :
          maxXAvailable = inHist.GetBinLowEdge(bin+1)+1
          maxBinAvailable = bin
          break

      # Min window width set at top. Max window width = half the distance between lowest and highest bins.
      maxWHW = int(math.floor(float(maxBinX-minBinX+1)/2.0))

      # Get start parameters I can trust
      fitSettings = functionLoopDict[function]
      startPars = startParDict[function][simplechannel][ntag]

      ## Cycle through available SWIFT fit options and generate configurations for each
      for whw in [0]+range(minWHW,maxWHW+1) :

        if whw > 0 :
          if not doSWIFTFits :
            continue
          else :
            extension = "whw{0}".format(whw)
            print "setting extension",extension

        if whw < 1 :
          if not doGlobalFits :
            continue
          else :
            extension = "global"

        newConfig = newConfigDir+"/Step1_SearchPhase_Swift_{0}_{1}_{2}_{3}.config".format(channel,ntag,function,extension)
        outputFile = outputDir+"SearchPhase_{0}_{1}_{2}_{3}.root".format(channel,ntag,function,extension)
        outputLog = "logs/SearchPhase_{0}_{1}_{2}_{3}.txt".format(channel,ntag,function,extension)
        with open(templateConfig,'r') as readConf :
          with open(newConfig,'w') as writeConf :

            for line in readConf :
              if "inputFileName" in line :
                line = "inputFileName {0}\n".format(inputFile)
              elif "outputFileName" in line :
                line = "outputFileName {0}\n".format(outputFile)
              elif "dataHist" in line :
                line = "dataHist {0}\n".format(inHistName)
              elif "minXForFit" in line :
                line = "minXForFit {0}\n".format(minXForFit)
              elif "maxXForFit" in line :
                line = "maxXForFit {0}\n".format(maxXForFit)
              elif "nPseudoExpFit" in line :
                line = "nPseudoExpFit    1\n"
              elif "functionCode" in line :
                line = "functionCode {0}\n".format(fitSettings["functioncode"])
              elif "nParameters" in line :
                line = "nParameters {0}\n".format(fitSettings["npar"])
              if whw > 0 :
                if "swift_minXAvailable" in line :
                  line = "swift_minXAvailable {0}\n".format(minXForFit)
                elif "swift_maxXAvailable" in line :
                  line = "swift_maxXAvailable {0}\n".format(maxXAvailable)
                elif "swift_nBinsLeft" in line :
                  line = "swift_nBinsLeft {0}\n".format(whw)
                elif "swift_nBinsRight" in line :
                  line = "swift_nBinsRight {0}\n".format(whw)
              else :
                if "doSwift" in line :
                  line = "doSwift False\n"
              # Want to keep the right number of parameters
              if "parameter1" in line :
                line = ""
                for par in sorted(startPars.keys()) :
                  line = line + "{0} {1}\n".format(par,startPars[par])
              elif "parameter" in line :
                continue
              writeConf.write(line)

        # Run the search
        if not useBatch :
          command = "SearchPhase --config {0} > {1}".format(newConfig,outputLog)
          print command
          subprocess.call(command,shell=True)
        else :
          print "Passing config name",newConfig
          batchmanager.send_job(newConfig)

